const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");

// API Get data users
userRouter.get("/users", userController.getAllUsers);

// API Sign Up/Register
userRouter.post("/users/signup", userController.userSignUp);

// API Login/Sign In
userRouter.post("/users/login", userController.userLogin);

// API Update and Insert biodata users
userRouter.put("/users/detail/:idUser", userController.updateUserBiodata);

// API Get biodata user
userRouter.get("/users/detail/:idUser", userController.getDetailUser);

// API Record game history
userRouter.post("/game", userController.recordGameHistory);

// API Get user's game history
userRouter.get("/users/game/history/:idUser", userController.getUserGameHistories);

module.exports = userRouter;
