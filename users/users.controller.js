const userModel = require("./users.model");

class UserController {
  // Get data users
  getAllUsers = async (req, res) => {
    const allUsers = await userModel.getAllUsers();
    res.json(allUsers);
  };

  // Sign Up
  userSignUp = async (req, res) => {
    const requestedData = req.body;
    // Check are username, email and password filled or empty
    if (requestedData.username === undefined || requestedData.username === "") {
      res.statusCode = 406;
      return res.json({ message: "Username is required!" });
    } else if (requestedData.email === undefined || requestedData.email === "") {
      res.statusCode = 406;
      return res.json({ message: "Email is required!" });
    } else if (
      requestedData.password === undefined ||
      requestedData.password === ""
    ) {
      res.statusCode = 406;
      return res.json({ message: "Password is required!" });
    }

    // Check have username and email already been registered
    const registeredUser = await userModel.isUserRegistered(requestedData);
    if (registeredUser) {
      res.statusCode = 400;
      return res.json({
        message:
          "This username or email is exist! Please use another username or email.",
      });
    }
    // Add new data user
    await userModel.createNewUser(requestedData);
    res.json({ message: "New user has been registered!" });
  };

  // login
  userLogin = async (req, res) => {
    const { username, password } = req.body;

    try {
      const dataUserLogin = await userModel.verifyLogin(username, password);
      // Check is data valid or invalid
      if (dataUserLogin) {
        return res.json(dataUserLogin);
      } else {
        res.statusCode = 400;
        return res.json({ message: "Username or password is invalid!" });
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ message: "User is not found! Something went wrong." });
    }
  };

  updateUserBiodata = async (req, res) => {
    const { idUser } = req.params;
    const requestedDataBio = req.body;

    await userModel.upsertUserBiodata(idUser, requestedDataBio);
    return res.json({ message: "User biodata updated!" });
  };

  getDetailUser = async (req, res) => {
    const { idUser } = req.params;
    try {
      const detailUser = await userModel.getBiodataUser(idUser);
      if (detailUser) {
        return res.json(detailUser);
      } else {
        res.statusCode = 400;
        return res.json({ message: "User biodata is not found!" });
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ message: "Opss.. Something went wrong!" });
    }
  };

  recordGameHistory = async (req, res) => {
    const recordingGame = req.body;
    const recordedGames = async (recordingGame) => {
      await userModel.recordGameHistory(recordingGame);
      if (recordingGame.result === "win") {
        return res.json({ message: `Player wins!` });
      } else if (recordingGame.result === "lose") {
        return res.json({ message: `Player loses!` });
      } else if (recordingGame.result === "draw") {
        return res.json({ message: `Draw! No Winner!` });
      } else if (
        recordingGame.result !== "win" ||
        recordingGame.result !== "lose" ||
        recordingGame.result !== "draw"
      ) {
        res.statusCode = 406;
        return res.json({
          message: `Opss.. Cannot record data! Something went wrong!`,
        });
      }
    };
    recordedGames(recordingGame);
  };

  getUserGameHistories = async (req, res) => {
    const { idUser } = req.params;
    try {
      const gameHistories = await userModel.getGameHistory(idUser);
      if (gameHistories) {
        return res.json(gameHistories);
      } else if (gameHistories === null) {
        res.statusCode = 400;
        return res.json({ message: "Cannot find game history!" });
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ message: "Opss.. Something went wrong!" });
    }
  };
}

module.exports = new UserController();
