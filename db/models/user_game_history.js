"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_Game_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_Game_History.belongsTo(models.User_Game, { foreignKey: "user_id" });
    }
  }
  User_Game_History.init(
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "User_Games",
          key: "id",
        },
      },
      player_choice: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      com_choice: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      result: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "User_Game_History",
    }
  );
  return User_Game_History;
};
