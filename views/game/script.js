// Button
const userChoice = document.querySelectorAll(".buttonP1");
const comChoice = document.querySelectorAll(".buttonCOM");
const resetButton = document.getElementById("buttonReset");

// Reset Button
resetButton.onclick = () => {
    userChoice.forEach((Element) => {
        Element.style.backgroundColor= "transparent";
        Element.style.pointerEvents= "";
    })
    comChoice.forEach((Element) => {
        Element.style.backgroundColor= "transparent";
    })
    versus.style.display = "";
    P1win.style.display = "none";
    COMwin.style.display = "none";
    drawResult.style.display = "none";
};

// Disable click button
const disableClick = () => {
    userChoice.forEach((element) => {
        element.style.pointerEvents= "none";
    })
}

// The Result
const versus = document.getElementById("vs");
const P1win = document.getElementById("P1win");
const COMwin = document.getElementById("COMwin");
const drawResult = document.getElementById("drawResult");

// Player and computer's choice
let player1ChoiceResult = "";
let computerChoiceResult = "";

// Decide the winner
const decideTheWinner = () => {
    // Player 1 WIN
    if ((player1ChoiceResult === "buttonRockP1" && computerChoiceResult === "buttonScissorsCOM") || 
    (player1ChoiceResult === "buttonPaperP1" && computerChoiceResult === "buttonRockCOM") || 
    (player1ChoiceResult === "buttonScissorsP1" && computerChoiceResult === "buttonPaperCOM")) {
        versus.style.display = "none";
        P1win.style.display = "flex";
        console.log("Player 1 WIN");
    }
    // COM WIN
      else if ((player1ChoiceResult === "buttonScissorsP1" && computerChoiceResult === "buttonRockCOM") || 
      (player1ChoiceResult === "buttonRockP1" && computerChoiceResult === "buttonPaperCOM") || 
      (player1ChoiceResult === "buttonPaperP1" && computerChoiceResult === "buttonScissorsCOM")) {
        versus.style.display = "none";
        COMwin.style.display = "flex";
        console.log("COM WIN");
    }
    // DRAW
      else {
        versus.style.display = "none";
        drawResult.style.display = "flex";
        console.log("DRAW")
      }
};

class Player {
    constructor(playerChoice) {
        this.playerChoice = playerChoice;
    }

    click() {
        this.playerChoice.forEach((choice) => {
            choice.onclick = () => {
                choice.style.backgroundColor= "#C4C4C4";
                player1ChoiceResult = choice.id;
                computer.computerChooses();
                disableClick();
            }
        });
    }
};

// Object player 1
const player1 = new Player(document.querySelectorAll(".buttonP1"));
player1.click();

class Computer {
    computerChooses = () => {
        const availableChoice = ["buttonRockCOM", "buttonPaperCOM", "buttonScissorsCOM"];
        const randomSelectedElement = Math.floor(Math.random() * 3);
        const computersChoice = availableChoice[randomSelectedElement];
        computerChoiceResult = availableChoice[randomSelectedElement];
        this.backgroundColorCOM(computersChoice);
        decideTheWinner();
    };

    backgroundColorCOM = (choiceCOM) => {
        const selectedElementCOM = document.getElementById (choiceCOM);
        selectedElementCOM.style.backgroundColor= "#C4C4C4";
    };
};

// Object computer
const computer = new Computer();