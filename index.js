const express = require("express");
const app = express();
const port = 3000;
const userRouter = require("./users/users.route");
// Middleware
app.use(express.json());
app.use(express.static("views"));

app.get("/", (req, res) => {
  return res.sendFile("./views/home/index.html");
});

app.use("/", userRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});